MAKEFLAGS += --jobs=$(shell getconf _NPROCESSORS_ONLN) 
# do not ouput invoked command but if $VERBOSE is non-null
$(VERBOSE).SILENT:

NAME=pandoc
FILES=pandoc.md
DEP=$(NAME).pandoc $(FILES) $(wildcard images/*.{jpg,jpeg,png,gif})

PANDOC_OPTIONS        = \
												--standalone \
							          --filter=pandoc-crossref --filter=pandoc-citeproc \
							          --toc-depth 2 

PANDOC_DOCX_OPTIONS   = # --reference-doc=FILE
PANDOC_ODT_OPTIONS    = # --reference-doc=FILE
PANDOC_HTML_OPTIONS   = --toc --metadata=numberSections:true\
												--css $(CURDIR)/stylesheets/johnmacfarlane.css \
										    --css $(CURDIR)/stylesheets/johnmacfarlane_personal.css \
										    --css $(CURDIR)/stylesheets/solarized.css \
												--self-contained --strip-comments --email-obfuscation=references
												# --mathml
PANDOC_LATEX_OPTIONS  = \
											--include-in-header $(CURDIR)/headers/latex/header.tex \
									   	--include-in-header $(CURDIR)/headers/latex/note.tex
											# --template scrlttr2
PANDOC_BEAMER_OPTIONS = \
											--include-in-header $(CURDIR)/headers/beamer/header.tex \
											--include-after-body ./after-bodies/latex/beamer.tex

# all: option used when calling 'make' on command line
all: beamer pdf
# revealjs: $(DEP)
# 	pandoc \
# 		$(PANDOC_OPTIONS) \
# 		$(PANDOC_DOCX_OPTIONS) \
# 		--from markdown --to revealjs \
# 		$(NAME).pandoc $(FILES) --output $(NAME).html
docx: $(DEP)
	pandoc \
		$(PANDOC_OPTIONS) \
		$(PANDOC_DOCX_OPTIONS) \
		--from markdown --to docx \
		$(NAME).pandoc $(FILES) --output $(NAME).docx
odt: $(DEP)
	pandoc \
		$(PANDOC_OPTIONS) \
		$(PANDOC_DOCX_OPTIONS) \
		--from markdown --to odt \
		$(NAME).pandoc $(FILES) --output $(NAME).odt
html: $(DEP)
	pandoc \
		$(PANDOC_OPTIONS) \
		$(PANDOC_HTML_OPTIONS) \
		--from markdown --to html5 \
		$(NAME).pandoc $(FILES) --output $(NAME).html
latex: $(DEP)
	pandoc \
		$(PANDOC_OPTIONS) \
		$(PANDOC_LATEX_OPTIONS) \
		--from markdown --to latex \
		$(NAME).pandoc $(FILES) --output $(NAME).tex
beamer: $(DEP)
	pandoc \
		$(PANDOC_OPTIONS) \
		$(PANDOC_BEAMER_OPTIONS) \
		--from markdown --to beamer \
		$(NAME).pandoc $(FILES) --output $(NAME).tex
pdf: ${NAME}.tex
	latexrun --color never $(NAME).tex

# all: option used when calling 'make run' on command line
run: run_pdf
run_docx: docx
	libreoffice --nologo $(NAME).docx \
	>/dev/null 2>&1 &
run_odt: odt
	libreoffice --nologo $(NAME).odt \
	>/dev/null 2>&1 &
run_html: html
	$(BROWSER) $(NAME).html
run_pdf:
	$(PDFVIEWER) $(NAME).pdf \
	>/dev/null 2>&1 &

clean: clean_docx clean_odt clean_html clean_pdf
clean_docx:
	rm --force --verbose *.docx
clean_odt:
	rm --force --verbose *.odt
clean_html:
	rm --force --verbose *.html
clean_pdf:
	latexrun --clean-all

check: check_latex
check_html:
	linkchecker $(NAME).html
check_latex:
	lacheck $(NAME).tex

.PHONY: all run clean check

#  ex:filetype=make 

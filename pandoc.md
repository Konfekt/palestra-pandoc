# `pandoc`: conversor entre linguagens de marcação

## O que é uma *Linguagem de Marcação* ?

Uma *linguagem de marcação* é uma linguagem para formatar o texto e as imagens de uma página.
Por exemplo, para formatar uma página visualizada pelo navegador, tal como o `Firefox`,

- a linguagem usada é `HTML` e a sua sintaxe, por exemplo:

```html
    "É <strong>Importante!</strong>"
```
- a linguagem `Markdown` compila a `HTML` e usa uma sintaxe mais intuitiva, por exemplo:

```markdown
    "É **Importante!**"
```

O resultado da formatação \quad $\leadsto$ \quad "É **Importante!**"


## O que é `pandoc` ?

`pandoc` é um programa (de linha de comando) que converte um arquivo de uma linguagem de marcação a outra:
De (entre outras)

- `markdown`, `HTML`, `LaTeX`, `Microsoft Word docx`, `reStructuredText`, `textile`, `OPML`, `Emacs Org-Mode`, `Txt2Tags`, `DocBook`, `EPUB` ou `Haddock markup`

. . .

a (entre outras)

- HTML: `XHTML`, `HTML5`, e eslaides por `Slidy`, `reveal.js`, `S5` ...
- Processador de Textos: `Microsoft Word docx`, `LibreOffice` e `OpenOffice ODT`, `OpenDocument XML`
- `PDF` via `LaTeX`
- Ebooks: `EPUB` version 2 or 3, `FictionBook2`
- Documentação: `DocBook`, `GNU TexInfo`, `Groff man`
- TeX: `LaTeX`, `ConTeXt`, eslaides `LaTeX Beamer`
<!-- - Light Markup: Markdown, reStructuredText, MediaWiki markup, DokuWiki markup, Emacs Org-Mode, Textile -->

## O que faz `pandoc` para mim ?

Converte arquivos do formato

- `markdown`

. . .

ao formato

- `HTML5`
- `Microsoft Word docx` , `LibreOffice ODT`, `OpenDocument XML`
- `LaTeX Beamer` eslaides
- `PDF` via `LaTeX`

## `Pandoc` versus Programas Especializados: O bem.

### Acessibilidade:
Código `markdown` é texto de fácil leitura.

### Em comparação com outras linguagens de marcação:

- A sintaxe `markdown` é mais simples do que a sintaxe `(La)TeX` (O inventor de `TeX`, Donald Knuth, espanta-se por quê demorou tanto para substituir `LaTeX` por outra linguagem de marcação leve que se compila a `TeX`).
- Em particular, a sintaxe `Markdown` é muito mais simples do que a sintaxe de `LaTeX Beamer`.
- Fórmulas matemáticas são mais facilmente escritas em `Markdown` que em `Microsft Word` ou `LibreOffice`,
- É especialmente apta para artigos curtos em `HTML`, como postagens de blog.

## `Pandoc` versus Programas Especializados: O mal.

- Funções especificas à linguagem de marcação, em `pandoc`,  
    - não podem ser usadas, ou
    - só podem ser usadas invalidando o output em outras linguagens.
    ($\implies$ a sintaxe de `markdown` é tão reduzida quanto a base comum entre as linguagens de marcação.)
    <!-- - Para documentos mais complexos, há certo consenso que o formato similar `asciidoc` é mais potente é por isso adequado. -->

. . .

- Pandoc está ainda sendo desenvolvido, por isso:
<!-- 	- o output as vezes não é tão acabado quanto se deseja, -->
    - a documentação é incompleta,
    - o ecossistema de programas para `pandoc`, tais como editores, é ainda incompleto:
    Por exemplo, comparando a `TeX`:
        - pode-se pular da posição do cursor no arquivo `TeX` à posição correspondente no arquivo `pdf` compilado , e vice-versa.
        Não há para `pandoc:` `markdown` compila a `TeX` e depois a `pdf`.
        - O plugin de `Vim` para `markdown` é recente e básico em comparação àquele para `LaTeX` que é estável e potente.


# sintaxe `markdown:` documentação = cola

## Cola

![Documentação (= Cola) da sintaxe de Markdown](images/cheatsheet.png "cheatsheet")

##
<!-- ## Input: -->

```markdown
# Itens alistados enfatizados:

- *peso*
- *altura*

#  Itens enumerados negritos:

1. **mãe**
0. **pai**

# Tabela

|        | mãe    | pai    |
|--------|--------|--------|
| peso   | 100 kg | 200 kg |
| altura | 1,20 m | 2,10 m |
```

## $\leadsto$

### Itens alistados enfatizados:

- *peso*
- *altura*

###  Itens enumerados negritos:

1. **mãe**
0. **pai**

### Tabela

|        | mãe    | pai    |
|--------|--------|--------|
| peso   | 100 kg | 200 kg |
| altura | 1,20 m | 2,10 m |

# compilar `pandoc:` Makefile e Mainfile

## Parâmetros `Pandoc`

```sh
General options:
  --from = FORMAT
   Specify input FORMAT such as markdown, rst, ..
  --to = FORMAT
     Specify output FORMAT such as html, LaTeX, ..
  --output = FILE
     Write output to FILE instead of stdout.

General writer options:
  --standalone
     Produce output with header and footer.
  --table-of-contents
     Include a generated table of contents in output.
  --self-contained
     Produce standalone HTML file without external dep.
```

## Makefile

O `Makefile` serve para:

- Compilar o input: por exemplo, o comando
    - `make pdf` gera o documento `pdf`,
    - `make docx` gera o documento `docx`,
    - `make html` gera o documento `HTML`.

. . .

- Olhar o output: por exemplo, o comando
    - `make run_pdf` mostra o documento `pdf`, e.g. em `Zathura`,
    - `make run_docx` mostra o documento `docx`,  e.g. em `LibreOffice`,
    - `make run_html` mostra o documento `HTML`, e.g. em `Firefox`.

##
<!-- ## Exemplo -->

```sh
NAME = palestra
FILES = $(NAME).pandoc intro.md conteudo.md resumo.md

PANDOC_OPTIONS = --stand-alone --filter pandoc-citeproc
PANDOC_LATEX_OPTIONS = --include-in-header header.tex

all: latex pdf
latex: $(FILES)
	pandoc $(PANDOC_OPTIONS) $(PANDOC_LATEX_OPTIONS) \
		--from markdown --to latex \
		$(NAME).pandoc $(FILES) --output $(NAME).tex
pdf: latex
	latexrun $(NAME).tex

run: run_pdf
run_pdf: pdf
	$(PDFVIEWER) $(NAME).pdf >/dev/null /2> &1 &
```

## Main file

O main file, o arquivo principal, estabelece

- o título,
- o autor, e
- a data.

. . .

Além disso,

- o idioma, pela opção `lang` que controla por exemplo o nome do résumo e das referências, e
- varias opções de `TeX`, tal como:

    - o tipo de documento, e
    - tamanho da fonte.

## Exemplo

```markdown
% Pandoc é bem massa!
% [Enno Nagel](mailto:epn@fsfe.org)
% CoAlTI, Hotel Premier Maceió, 28 Outubro 2017

---
lang:                 pt

# latex:
documentclass:        scrartcl
classoption:          final,DIV = calc,bibliography = totoc
fontsize:             12pt
bibliography:         pandoc.bib
...
```

## Configurando o editor Vim

### Compilação automática

Para o editor Vim compilar após guardar o arquivo, acrescente ao arquivo `~/.vim/after/ftplugin/markdown.vim` a linha:

```vim
autocmd BufWrite <buffer> silent make!
```

Por exemplo, se o output é

- `pdf` (via TeX), então o amostrador `zathura` automaticamente recarrega o arquivo modificado,
- `html`, então a extensão `autoreload` do navegador `Firefox` automaticamente recarrega o arquivo modificado.

<!-- ### Plug-In -->
<!--  -->
<!-- O plug-in `vim-pandoc` -->
<!--  -->
<!-- - completa referências na biblioteca `BibTeX` pela tecla ` < Tab > `. -->
<!-- - dobra seções e trechos de código, -->
<!-- - dá um resumo dos arquivos. -->
<!--  -->
<!-- ### Modelos de arquivos -->
<!--  -->
<!-- O plug-in `vim-template` enche, quando abrindo -->
<!--  -->
<!-- - um novo `makefile`, o arquivo pelo código do `makefile` acima, e -->
<!-- - um novo `pandoc` file, o arquivo pelo código do main file acima. -->

## Site e Repositório

Estão disponíveis

- os eslaides,
- um resumo e
- a cola

em \quad <https://konfekt.bitbucket.io/talks/pandoc>,

e

- o `Makefile` e
- o código fonte (simples!) para compilar estes eslaides

no repositório \quad <https://www.bitbucket.org/konfekt/talks/pandoc>

<!--  ex: set spelllang=pt:  -->

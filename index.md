# como-usar-pandoc

Notas da palestra FLISoL, CESMAC Maceió, 8. Abril 2017:

Inclui

- o *input* na pasta principal:
    - o makefile = `makefile`
    - o arquivo principal `main.pandoc`, e
    - o código `pandoc.md` (e a pasta contendo as imagens `images`),
- o *output*, na pasta principal: o arquivo `main.*` com
    - `*` = `pdf`
    - `*` = `html`
    - `*` = `docx`
    - `*` = `odt`
- os cabeçalhos injetadas no arquivo de `TeX` compilado por `pandoc`, nas pastas `headers`, e

- a documentação da sintaxe Markdown no arquivo `material/Markdown Cheatsheet.pdf`
